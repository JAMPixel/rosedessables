import pygame
import math as m
from Objet import *

SPEED=0.5

class Ennemy(pygame.sprite.Sprite):
    def __init__(self, x, y, x_map, y_map, player, carte, matiere):
        pygame.sprite.Sprite.__init__(self)
        self.matiere=matiere
        self.static=pygame.image.load("images/ennemi"+str(self.matiere)+".png").convert_alpha()
        self.image=self.static.subsurface(pygame.Rect(0,0,24,32))

        self.rect=self.image.get_rect(center=(x+self.image.get_width()/2,y+self.image.get_height()/2))
        self.dx=0
        self.dy=0
        if self.matiere==4:
            self.pv=6
        else:
            self.pv=3
        self.carte=carte
        self.x=self.rect.x
        self.y=self.rect.y
        self.x_map=x_map
        self.y_map=y_map
        self.player=player
        self.collide=False
        self.inv=False
        self.compt_blink=0
        self.orientation=0

    def update(self,x_fond,y_fond):
        # self.dx=self.player.x-self.x_map
        # self.dy=self.player.y-self.y_map
        self.dx=self.player.rect.x-self.x
        self.dy=self.player.rect.y-self.y
        norme=m.sqrt(self.dx**2+self.dy**2)
        if norme< 200:
            self.dx=SPEED*self.dx/norme
            self.dy=SPEED*self.dy/norme
        else:
            self.dx=0
            self.dy=0

        if self.dy>0:
            self.orientation=0
        elif self.dy<0:
            self.orientation=1
        if self.dx>0 and self.dx>abs(self.dy):
            self.orientation=3
        elif self.dx<0 and abs(self.dx)>abs(self.dy):
            self.orientation=2

        if self.inv:
            self.recul()

        i=int(self.x_map//48)
        j=int((self.y_map+10)//48)
        i_gauche=int((self.x_map+self.dx)//48)
        i_droite=int((self.x_map+self.dx+self.image.get_width())//48)
        j_haut=int((self.y_map+self.dy+10)//48)
        j_bas=int((self.y_map+self.dy+self.image.get_height())//48)


        if self.carte[j][i_gauche]==2 or self.carte[j][i_droite]==2:
            self.dx=0

        if self.carte[j_haut][i]==2 or self.carte[j_bas][i]==2:
            self.dy=0

        self.x+=self.dx
        self.y+=self.dy
        self.x_map+=self.dx
        self.y_map+=self.dy
        self.rect.x=self.x
        self.rect.y=self.y

    def reverse(self):
        self.x-= 2*self.dx
        self.y-= 2*self.dy
        self.rect.x=self.x
        self.rect.y=self.y

    def degat(self, degats):
        self.pv-=degats
        self.inv=True

    def recul(self):
        if self.compt_blink<30:
            self.dx=-8*self.dx
            self.dy=-8*self.dy

    def draw(self,fenetre):
        self.image=self.static.subsurface(pygame.Rect(self.orientation*self.image.get_width(),0,24,32))
        k=self.compt_blink//15
        if not self.inv or (self.inv and (k==1 or k==3 or k==5)):
            fenetre.blit(self.image, (self.x, self.y))

        if self.inv:
            self.compt_blink+=1

        if self.compt_blink==105:
            self.compt_blink=0
            self.inv=False
