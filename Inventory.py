import pygame
from pygame.locals import *
from Perso import *

def Inventory(pause, player, fenetre):
    fond=fenetre.copy()
    opacity=fond.copy()
    rect_pause=fenetre.get_rect()
    opacity.set_alpha(150)

    pygame.draw.rect(opacity, (0,0,0), rect_pause)

    l_pioche=[]
    l_pioche.append(pygame.image.load("images/pioche.png").convert_alpha())
    l_pioche.append(pygame.image.load("images/piocheo.png").convert_alpha())
    l_pioche.append(pygame.image.load("images/piocheo.png").convert_alpha())
    l_pioche.append(pygame.image.load("images/piochec.png").convert_alpha())


    im_min_fer=pygame.image.load("images/mineraifer.png").convert_alpha()
    im_min_or=pygame.image.load("images/mineraior.png").convert_alpha()
    im_min_diam=pygame.image.load("images/mineraidiam.png").convert_alpha()

    minerai=[im_min_fer,im_min_or,im_min_diam]

    im_fleche_g=pygame.image.load("images/flecheg.png").convert_alpha()
    im_fleche_d=pygame.image.load("images/fleched.png").convert_alpha()
    im_selection=im_min_fer

    im_forge=pygame.image.load("images/Bouttonforge.png").convert_alpha()

    width=fenetre.get_width()
    height= fenetre.get_height()

    sel=0

    police = pygame.font.Font ("upcll.ttf", 50)
    txtf = police.render (str(player.compt_fer), 1, (255,255,255))
    txto = police.render (str(player.compt_or), 1, (255,255,255))
    txtd = police.render (str(player.compt_diam), 1, (255,255,255))

    police2 = pygame.font.Font ("upcll.ttf", 50)

    txtcout= police.render ("Cout: 5", 1, (255,255,255))
    txtvie=police.render("Vie: "+str(player.pv), 1, (255,255,255))

    while pause:
        if player.pv_pioche==0:
            im_pioche=l_pioche[-1]
        else:
            im_pioche=l_pioche[player.type_pioche]

        txtviepioche=police.render("Nombre d'utilisations restantes: "+str(player.pv_pioche), 1, (255,255,255))
        if sel==0:
            dur=30
            mine=1
            degat=1
        elif sel==1:
            dur=20
            mine=2
            degat=1
        elif sel==2:
            dur=40
            mine=1
            degat=2

        txteffdur= police.render ("durabilite: "+str(dur), 1, (255,255,255))
        txteffmin= police.render ("minerai: x"+str(mine), 1, (255,255,255))
        txteffdeg= police.render ("degat: "+str(degat), 1, (255,255,255))

        fenetre.blit(fond,(0,0))
        fenetre.blit(opacity,(0,0))
        fenetre.blit(im_pioche, (width/4-64,height/4-64))

        fenetre.blit(im_min_fer, (3*width/4-32, height/4-64))
        fenetre.blit(txtf, (3*width/4+16, height/4-64))

        fenetre.blit(im_min_or, (3*width/4-32, height/4))
        fenetre.blit(txto, (3*width/4+16, height/4))

        fenetre.blit(im_min_diam, (3*width/4-32, height/4+64))
        fenetre.blit(txtd, (3*width/4+16, height/4+64))

        fenetre.blit(txtvie, (width/4-64, height/2-32))
        fenetre.blit(txtviepioche, (width/4-64, height/2 ))

        fenetre.blit(im_fleche_g, (width/4-32-24,3*height/4-128))
        fenetre.blit(im_fleche_d, (width/4+32,3*height/4-128))
        fenetre.blit(im_selection, (width/4-16,3*height/4-128-4))

        fenetre.blit(txtcout,(width/4-64,3*height/4-64))
        fenetre.blit(im_selection, (width/4+50,3*height/4-64))
        fenetre.blit(txteffdur, (width/4-32,3*height/4))
        fenetre.blit(txteffmin, (width/4-32,3*height/4+72))
        fenetre.blit(txteffdeg, (width/4-32,3*height/4+144))

        fenetre.blit(im_forge, (3*width/4-64,3*height/4-32))



        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and (event.key == K_ESCAPE or event.key == K_o)):
                pause = False
            if event.type==KEYDOWN and event.key==K_LEFT:
                sel=(sel-1)%len(minerai)
                im_selection=minerai[sel]
            if event.type==KEYDOWN and event.key==K_RIGHT:
                sel=(sel+1)%len(minerai)
                im_selection=minerai[sel]
            if event.type==KEYDOWN and event.key==K_SPACE:
                if sel==0 and player.compt_fer>=5:
                    player.compt_fer-=5
                    player.pv_pioche=30
                    player.type_pioche=0
                    txtf = police.render (str(player.compt_fer), 1, (255,255,255))
                elif sel==1 and player.compt_or>=5:
                    player.compt_or-=5
                    player.pv_pioche=20
                    player.type_pioche=1
                    txto = police.render (str(player.compt_or), 1, (255,255,255))
                elif sel==2 and player.compt_diam>=5:
                    player.compt_diam-=5
                    player.pv_pioche=40
                    player.type_pioche=2
                    txteffdeg= police.render ("degat: "+str(degat), 1, (255,255,255))





        pygame.display.flip()

    return(pause,player)
