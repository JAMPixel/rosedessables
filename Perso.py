import pygame

SPEED=0.5

class Perso(pygame.sprite.Sprite):
    def __init__(self, largeur, hauteur, x, y, matrice):
        pygame.sprite.Sprite.__init__(self)
        self.static=pygame.image.load("images/mineur.png").convert_alpha()
        self.image=self.static.subsurface(pygame.Rect(0,0,24,32))
        self.rect=self.image.get_rect(center=(largeur/2, hauteur/2))
        self.x=x+self.image.get_width()/2
        self.y=y+self.image.get_height()/2
        self.carte=matrice
        self.dx=0
        self.dy=0
        self.dx_recul=0
        self.dy_recul=0
        self.pv=10
        self.pv_pioche=30
        self.compt_fer=0
        self.compt_or=0
        self.compt_diam=0

        self.pioche_anim_vert=pygame.image.load("images/piocheverticale.png").convert_alpha()
        self.pioche_anim_hori=pygame.image.load("images/piochehorizontale.png").convert_alpha()
        self.pioche_im_vert=self.pioche_anim_vert.subsurface(pygame.Rect(0, 0, 16, 24))
        self.pioche_im_hori=self.pioche_anim_hori.subsurface(pygame.Rect(0, 0, 24, 16))
        self.pioche=pygame.Rect(self.x+4, self.y+self.image.get_height()+8, 16, 16)
        self.type_pioche=0

        self.frappe=False
        self.inv=False
        self.compt_frappe=0
        self.compt_walk=0
        self.compt_blink=0
        self.orientation=0 #(droite=0, bas=1, gauche=2, haut=3)

    def update(self, groupe, liste):
        if self.dy>0:
            self.orientation=0
        elif self.dy<0:
            self.orientation=1
        if self.dx>0:
            self.orientation=3
        elif self.dx<0:
            self.orientation=2

        if self.inv:
            self.recul()

        i=int(self.x//48)
        j=int((self.y+10)//48)
        i_gauche=int((self.x+self.dx)//48)
        i_droite=int((self.x+self.dx+self.image.get_width())//48)
        j_haut=int((self.y+self.dy+10)//48)
        j_bas=int((self.y+self.dy+self.image.get_height())//48)

        if self.carte[j][i_gauche]==2 or self.carte[j][i_droite]==2 or (self.carte[j_bas][i_gauche]==2 and not self.carte[j][i_gauche]==1) or (self.carte[j_bas][i_droite]==2 and not self.carte[j][i_droite]==1):
            self.dx=0

        if self.carte[j_haut][i]==2 or self.carte[j_bas][i]==2 or (self.carte[j_haut][i_gauche]==2 and not self.carte[j_haut][i]==1) or self.carte[j_haut][i_droite]==2 or self.carte[j_bas][i_droite]==2:
            self.dy=0

        for sprite in groupe:
            sprite.x-=self.dx
            sprite.y-=self.dy
            sprite.rect.x=sprite.x
            sprite.rect.y=sprite.y

        for obj in liste:
            obj.x=obj.x-self.dx
            obj.y=obj.y-self.dy
            obj.rect.x=obj.x
            obj.rect.y=obj.y

        self.x+=self.dx
        self.y+=self.dy


    def update_pioche(self):
        k=self.compt_frappe//20
        if self.orientation==3:
            self.pioche.x=self.rect.x+self.image.get_width()+8
            self.pioche.y=self.rect.y+8
            self.pioche_im_hori=self.pioche_anim_hori.subsurface(pygame.Rect(k*24, 0, 24, 16))
            self.pioche_im_hori=pygame.transform.flip(self.pioche_im_hori, True, False)
        elif self.orientation==0:
            self.pioche.x=self.rect.x+4
            self.pioche.y=self.rect.y+self.image.get_height()+8
            self.pioche_im_vert=self.pioche_anim_vert.subsurface(pygame.Rect(k*16, 0, 16, 24))
            self.pioche_im_vert=pygame.transform.flip(self.pioche_im_vert, False, True)
        elif self.orientation==2:
            self.pioche.x=self.rect.x-self.pioche.width-8
            self.pioche.y=self.rect.y+8
            self.pioche_im_hori=self.pioche_anim_hori.subsurface(pygame.Rect(k*24, 0, 24, 16))
        elif self.orientation==1:
            self.pioche.x=self.rect.x+4
            self.pioche.y=self.rect.y-self.pioche.height-8
            self.pioche_im_vert=self.pioche_anim_vert.subsurface(pygame.Rect(k*16, 0, 16, 24))


    def degat(self):
        self.pv-=1
        self.inv=True

    def recul(self):
        if self.compt_blink<30:
            self.dx=-15*SPEED*self.dx_recul
            self.dy=-15*SPEED*self.dy_recul
        else:
            self.dx_recul=0
            self.dy_recul=0

    def draw(self, fenetre):
        self.image=self.static.subsurface(pygame.Rect(self.orientation*self.image.get_width(),0,24,32))
        k=self.compt_blink//15
        if not self.inv or (self.inv and (k==1 or k==3 or k==5)):
            fenetre.blit(self.image, (self.rect.x, self.rect.y))

        if self.frappe:
            if self.orientation==0:
                fenetre.blit(self.pioche_im_vert, (self.pioche.x, self.pioche.y-8))
            if self.orientation==1:
                fenetre.blit(self.pioche_im_vert, (self.pioche.x, self.pioche.y))
            if self.orientation==2:
                fenetre.blit(self.pioche_im_hori, (self.pioche.x, self.pioche.y))
            if self.orientation==3:
                fenetre.blit(self.pioche_im_hori, (self.pioche.x-8, self.pioche.y))
            self.compt_frappe+=1
            if self.compt_frappe==60:
                self.compt_frappe=0
                self.frappe=False



        if self.inv:
            self.compt_blink+=1

        if self.compt_blink==105:
            self.compt_blink=0
            self.inv=False
