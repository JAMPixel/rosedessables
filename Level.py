import pygame
import random
import math as m
from pygame.locals import *
from Perso import *
from Ennemy import *
from Inventory import *
from Donjon import *
from Objet import *

SPEED=0.5
Taille_bloc=48

def game_scene(fenetre):
    win_w=fenetre.get_width()
    win_h=fenetre.get_height()
    donjon=gendonjon()
    noir=pygame.Surface((win_w,win_h))
    noir.fill((0,0,0))

    police = pygame.font.Font ("upcll.ttf", 50)
    im_min_fer=pygame.image.load("images/mineraifer.png").convert_alpha()
    im_min_or=pygame.image.load("images/mineraior.png").convert_alpha()
    im_min_diam=pygame.image.load("images/mineraidiam.png").convert_alpha()

    bruit_pioche=pygame.mixer.Sound("bruitages/Coup_vide0.wav")
    bruit_eboulement=pygame.mixer.Sound("bruitages/Eboulement.wav")
    pygame.mixer.music.load("bruitages/Hey_ho.mp3")
    pygame.mixer.music.play(-1)


    fond, l_spawn=map_gen(donjon)
    x_fond,y_fond=recherche_depart(donjon)

    mineur=Perso(win_w, win_h, x_fond*Taille_bloc, y_fond*Taille_bloc, donjon)

    y_fond=-(y_fond*Taille_bloc+mineur.image.get_height()-win_h/2)
    x_fond=-(x_fond*Taille_bloc+mineur.image.get_width()-win_w/2)

    ennemies=pygame.sprite.RenderUpdates()
    game = True
    pause = False
    compt=0

    listeObjet=[]

    while game:
        fenetre.blit(noir,(0,0))
        mineur.dx=0
        mineur.dy=0


        for (i,j) in l_spawn:
            distance=m.sqrt((Taille_bloc*i-mineur.x)**2+(Taille_bloc*j-mineur.y)**2)
            if distance<6*Taille_bloc:
                pop_ennemy=random.random()
                if pop_ennemy<0.001:

                    x_ennemy=i*Taille_bloc+(win_w/2)-mineur.x
                    y_ennemy=j*Taille_bloc+(win_h/2)-mineur.y
                    matiere=random.random()
                    if matiere<0.5:
                        matiere=1
                    elif matiere<0.75:
                        matiere=2
                    elif matiere<0.92:
                        matiere=3
                    else:matiere=4
                    ennemies.add(Ennemy(x_ennemy,y_ennemy,i*Taille_bloc, j*Taille_bloc, mineur, donjon, matiere))

        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                game = False

        if not pause:
            key_list=pygame.key.get_pressed()

            if key_list[K_LEFT]:
                mineur.dx-=SPEED
            if key_list[K_RIGHT]:
                mineur.dx+=SPEED
            if key_list[K_UP]:
                mineur.dy-=SPEED
            if key_list[K_DOWN]:
                mineur.dy+=SPEED
            if key_list[K_SPACE]:
                if mineur.compt_frappe==0:
                    bruit_pioche.play()
                mineur.frappe=True
            if key_list[K_p]:
                pause=True
                (pause,mineur)=Inventory(pause, mineur, fenetre)

            mineur.update(ennemies, listeObjet)
            ennemies.update(x_fond,y_fond)

        x_fond-=mineur.dx
        y_fond-=mineur.dy
        # for obj in listeObjet:
        #     obj.rect.x-=mineur.dx
        #     print(mineur.dx)
        #     obj.rect.y-=mineur.dy

        fenetre.blit(fond,(x_fond,y_fond))


        liste_collide=pygame.sprite.spritecollide(mineur,ennemies,False)
        for ennemy in liste_collide:
            ennemy.reverse()
            if not mineur.inv:
                mineur.degat()
                mineur.dx_recul=-1*ennemy.dx
                mineur.dy_recul=-1*ennemy.dy
                if mineur.pv<=0:
                    game=False


        l_colide_obj=pygame.sprite.spritecollide(mineur,listeObjet,False)
        for obj in l_colide_obj:
            if obj.type==0:
                mineur.compt_fer+=1
            elif obj.type==1:
                mineur.compt_or+=1
            elif obj.type==2:
                mineur.compt_diam+=1

            listeObjet.remove(obj)


        mineur.draw(fenetre)

        if mineur.frappe:
            if donjon[int((mineur.y+12)//48)][int(mineur.x//48)]==5:
                game=False
            mineur.update_pioche()
            for ennemy in ennemies:

                if mineur.pioche.colliderect(ennemy.rect):
                    if not ennemy.inv:
                        if mineur.pv_pioche==0:
                            ennemy.degat(0.5)
                        else:
                            if mineur.type_pioche==0 or mineur.type_pioche==1:
                                ennemy.degat(2)
                            elif mineur.type_pioche==2:
                                ennemy.degat(3)
                            mineur.pv_pioche-=1
                    if ennemy.pv<=0:
                        if not ennemy.matiere==1:
                            if mineur.type_pioche==1:
                                listeObjet.append(Objet(ennemy.matiere-2,ennemy.x+4, ennemy.y, mineur))
                                listeObjet.append(Objet(ennemy.matiere-2,ennemy.x+4, ennemy.y+16, mineur))
                            else:
                                listeObjet.append(Objet(ennemy.matiere-2,ennemy.x+4, ennemy.y+8, mineur))
                        ennemy.kill()
                        bruit_eboulement.play()


        for ennemy in ennemies:
            ennemy.draw(fenetre)

        for obj in listeObjet:
            obj.draw(fenetre)

        txtvie = police.render("Vie : " + str(mineur.pv), 1, (255,255,255))
        txtviepioche = police.render("Pioche : " + str(mineur.pv_pioche), 1, (255,255,255))
        txtf = police.render (str(mineur.compt_fer), 1, (255,255,255))
        txto = police.render (str(mineur.compt_or), 1, (255,255,255))
        txtd = police.render (str(mineur.compt_diam), 1, (255,255,255))


        pygame.draw.rect(fenetre, (0,0,0), Rect(0, 0, win_w, 60))
        fenetre.blit(txtvie, (win_w/16, 10))
        fenetre.blit(txtviepioche, (win_w/4, 10))
        fenetre.blit(im_min_fer, (win_w/2, 10))
        fenetre.blit(txtf, (win_w/2 + 40, 0))
        fenetre.blit(im_min_or, (2*win_w/3, 10))
        fenetre.blit(txto, (2*win_w/3 + 40, 0))
        fenetre.blit(im_min_diam, (5*win_w/6, 10))
        fenetre.blit(txtd, (5*win_w/6 + 40, 0))

        pygame.display.flip()


    compteur=mineur.compt_fer*1 +mineur.compt_or*10 +mineur.compt_diam*100
    if mineur.pv==0:
        compteur= int(compteur//2)


    txt = police.render ("votre score est de:", 1, (255,255,255))

    police2 = pygame.font.Font ("upcll.ttf", 100)
    txt2 = police2.render (str(compteur), 1, (255,255,255))
    score=True
    while score:
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and (event.key == K_ESCAPE or event.key==K_SPACE)):
                score = False
        fenetre.blit(noir,(0,0))

        fenetre.blit(txt, (50,100))
        fenetre.blit(txt2, (50,300))

        pygame.display.flip()
