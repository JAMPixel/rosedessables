# RoseDesSables

Requis : Python 3.2, Pygame

tapez : python main.py

Commandes : 
Sur l'écran titre :
	Barre espace : démarrer
	Echap : Quitter jeu

Dans le jeu : 
	Flèches directionnelles : déplacement
	Barre espace : attaquer
	touche p : Inventaire
	Echap : Quitter jeu

Dans l'inventaire :
	Flèche gauche/droite : changer minerai
	Barre espace : forger pioche
	touche o : retour au jeu
	Echap : Quitter jeu


