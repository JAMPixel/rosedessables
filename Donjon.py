
import random
import os
import pygame
from pygame.locals import *

def gendonjon():
    nb=random.randint(0,299)

    texte=""

    with open("Map/map"+str(nb)+".txt",'r') as fichier:
    # with open("Map/maptest.txt",'r') as fichier:
        for line in fichier:
            texte += line


    texte=texte.split("\n")
    for k in range(0,len(texte)):
        texte[k]=texte[k].split()

    donj=[]

    for i in range(0,len(texte)):
        donj.append([])
        for j in range(0,len(texte[i])):
            donj[i].append(int(texte[i][j]))

    return donj

Taille_bloc=48


def map_gen(carte):

    l_spawn=[]
    Surf_map = pygame.Surface((len(carte[0])*Taille_bloc,len(carte)*Taille_bloc))

    Escalier=pygame.image.load("images/escalier.png").convert_alpha()
    Mur=pygame.image.load("images/mur.png").convert_alpha()
    Air=pygame.image.load("images/sol.png").convert_alpha()
    Spawn=pygame.image.load("images/spawn.png").convert_alpha()

    Vide= pygame.Surface((48,48))
    Vide.fill((0,0,0))

    for i in range(0,len(carte)):
        for j in range(0,len(carte[i])):
            if carte[i][j]==1 or carte[i][j]==3 or carte[i][j]==4:
                Bloc=Air
            elif carte[i][j]==2:
                Bloc=Mur
            elif carte[i][j]==5:
                Bloc=Escalier
            elif carte[i][j]==6:
                Bloc=Spawn
                l_spawn.append((j,i))
            else:
                Bloc=Vide

            Surf_map.blit(Bloc, (j*Taille_bloc,i*Taille_bloc))
    return Surf_map,l_spawn


def recherche_depart(carte):
    for i in range(len(carte)):
        for j in range(len(carte[i])):
            if carte[i][j]==4:
                x=j
                y=i
    return (x,y)
