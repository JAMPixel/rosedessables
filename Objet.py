import pygame

SPEED=0.5

class Objet(pygame.sprite.Sprite):
    def __init__(self, typ, x,y,player):
        pygame.sprite.Sprite.__init__(self)
        self.type=typ
        if typ==0:
            self.image=pygame.image.load("images/mineraifer.png").convert_alpha()
        elif typ==1:
            self.image=pygame.image.load("images/mineraior.png").convert_alpha()
        else :
            self.image=pygame.image.load("images/mineraidiam.png").convert_alpha()
        self.image=pygame.transform.scale(self.image, (16,16))
        self.rect=self.image.get_rect(center=(x+8,y+8))
        self.x=self.rect.x
        self.y=self.rect.y
        self.player = player

    def update(self):
        self.rect.x-=self.player.dx*SPEED
        self.rect.y-=self.player.dy*SPEED

    def draw(self, fenetre):
        fenetre.blit(self.image, (self.x, self.y))
