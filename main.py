import pygame
from pygame.locals import *
from Level import *

pygame.init()

if pygame.display.get_init():
	fenetre=pygame.display.set_mode([0,0],FULLSCREEN)


titre= pygame.image.load("images/titre.png").convert()

affichage=True


while affichage:
        fenetre.blit(titre,(0,0))
        pygame.display.flip()

        for event in pygame.event.get():
                if event.type==QUIT:
                        affichage=False
                elif event.type==KEYDOWN and event.key==K_ESCAPE:
                        affichage=False
                elif event.type==KEYDOWN and event.key==K_SPACE:
                        game_scene(fenetre)
                        affichage=False

pygame.quit()
